﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prm.Domain.Models
{
    /// <summary>
    /// Charge
    /// </summary>
    public class Charge
    {
        [Key]
        public string ChargeId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public bool Valid { get; set; }
        public DateTime Date { get; set; }
    }
}

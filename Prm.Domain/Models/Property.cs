﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prm.Domain.Models
{
    public class Property
    {
        [Key]
        public int PropertyId { get; set; }
        public int PropertyOwerId { get; set; }
        public string Name { get; set; }
        public string Descriptiion { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalCost { get; set; }
        public string Image { get; set; }
        public string ChargeIds { get; set; }
        public bool HasCharges { get; set; }
        public DateTime Date { get; set; }
        public DateTime AcquiredDate { get; set; }
    }
}

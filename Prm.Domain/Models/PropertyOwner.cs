﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prm.Domain.Models
{
   public class PropertyOwner
    {
        [Key]
        public int PropertyOwnerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public int Status { get; set; }
        public  DateTime Date { get; set; }
             
    }
}

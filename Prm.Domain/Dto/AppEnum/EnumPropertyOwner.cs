﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prm.Domain.Dto.AppEnum
{
    enum EnumPropertyOwner
    {
        Registered,
        Approved,
        Enable,
        Suspended,        
        Disable,
    }
}

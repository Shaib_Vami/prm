﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Prm.Web.Startup))]
namespace Prm.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

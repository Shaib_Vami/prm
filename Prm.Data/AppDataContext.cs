﻿using Microsoft.AspNet.Identity.EntityFramework;
using Prm.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prm.Data
{
   public class AppDataContext: IdentityDbContext<AppUser>
    {
        static AppDataContext contextInstance;

        private AppDataContext() : base("propertmgrconnectionstring") { }

        public static AppDataContext Create()
        {
            contextInstance = new Data.AppDataContext();
            if (contextInstance == null)
            {
                Database.SetInitializer<AppDataContext>(new DbInitializer());
                contextInstance = new AppDataContext();
            }

            return contextInstance;
        }

        public static AppDataContext GetInstance()
        {
            //if (contextInstance == null)
            //    contextInstance = new Data.AppDbContext();

            return contextInstance;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
        }



        public DbSet<PropertyOwner> PropertyOwner { get; set; }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prm.Data
{
    public class DbInitializer : DropCreateDatabaseIfModelChanges<AppDataContext>
    {
        protected override void Seed(AppDataContext context)
        {
            //add any database initialization code here

            //create some business plans

            //context.BusinessPlans.AddRange(new List<BusinessPlan>
            //{
            //    new BusinessPlan {  Investment = 15000, Name="Wood", Ranking = 1, Returns=  60000},
            //   new BusinessPlan {  Investment = 30000, Name="Copper", Ranking = 2, Returns=  120000},
            //   new BusinessPlan {  Investment = 60000, Name="Bronze", Ranking = 3, Returns=  240000},
            //    new BusinessPlan {  Investment = 120000, Name="Silver", Ranking = 4, Returns=  480000},
            //     new BusinessPlan {  Investment = 240000, Name="Gold", Ranking = 5, Returns=  960000},
            //      new BusinessPlan {  Investment = 480000, Name="Diamond", Ranking = 6, Returns=  1920000},
            //       new BusinessPlan {  Investment = 960000, Name="Platinum", Ranking = 7, Returns=  2840000}
            //});

            //context.SaveChanges();
        }

    }
}
